#include "Scheduler.h"
#include "MsgService.h"
#include "MoveTask.h"

Scheduler sched;

void setup(){
  
  sched.init(25);

  MsgService.init();
    
  MoveTask* pMoveTask = new MoveTask();
  pMoveTask->init(25);
  sched.addTask(pMoveTask);
}

void loop(){
  sched.schedule();
}
