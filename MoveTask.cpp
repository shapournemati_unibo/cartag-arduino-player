#include "MoveTask.h"
#include "Sonar.h"
#include "config.h"
#include "MsgService.h"
#include "Logger.h"
#include "SoftwareSerial.h"

#define enA 9
#define in1 4
#define in2 5
#define enB 10
#define in3 6
#define in4 7

int motorSpeedA = 0;
int motorSpeedB = 0;

bool connected = false;

SoftwareSerial btChannel(2, 3);

MoveTask::MoveTask(){
}
  
void MoveTask::init(int period){

  pinMode(enA, OUTPUT);
  pinMode(enB, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);
  
  btChannel.begin(9600);
  Task::init(period);
  Logger.log("RT:INIT");
}
  
void MoveTask::tick(){

  int xAxis = 0;
  int yAxis = 0;
  byte leftSpeed = 2;
  byte rightSpeed = 2;
  byte buffer[6]={0,0,0,0,0,0};

  if (btChannel.available()) {
    connected = true;
    btChannel.readBytes(buffer,6);
    for (int i = 0; i < 6; i++) {
      if (buffer[i] == 'L') {
        Serial.print('L');
        Serial.println(buffer[++i]);      
        leftSpeed = buffer[i];
      }
      if (buffer[i] == 'R') {
        Serial.print('R');
        Serial.println(buffer[++i]);      
        rightSpeed = buffer[i];
      }
      if (buffer[i] == 'S') {
        Serial.print('S');
        Serial.println(buffer[++i]);      
      }
    }
  }
  //int xAxis = analogRead(A0); // Read Joysticks X-axis
  //int yAxis = analogRead(A1); // Read Joysticks Y-axis

  if (connected) {
  
  if (rightSpeed < 2) {
    // Set Motor A backward
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
    motorSpeedA = map(rightSpeed, 1, 0, 125, 250);
  } else if (rightSpeed> 2) {
    // Set Motor A forward
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
    motorSpeedA = map(rightSpeed, 3, 4, 125, 250);
  } else {
    motorSpeedA = 0;
  }
  
  if (leftSpeed < 2) {
    // Set Motor B backward
    digitalWrite(in3, HIGH);
    digitalWrite(in4, LOW);
    motorSpeedB = map(leftSpeed, 1, 0, 125, 250);
  } else if (leftSpeed > 2) {
    // Set Motor B forward
    digitalWrite(in3, LOW);
    digitalWrite(in4, HIGH);
    motorSpeedB = map(leftSpeed, 3, 4, 125, 250);
  } else {
    motorSpeedB = 0;
  } 
  
  analogWrite(enA, motorSpeedA); // Send PWM signal to motor A
  analogWrite(enB, motorSpeedB); // Send PWM signal to motor B      
  connected = false;
  }

}



