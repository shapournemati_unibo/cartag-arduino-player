#ifndef __MOVE_TASK__
#define __MOVE_TASK__

#include "Task.h"
#include "ProximitySensor.h"
#include "Led.h"

class MoveTask: public Task {

public:
  MoveTask();
  void init(int period);  
  void tick();
};

#endif

